import Axios from 'axios';
import { Client, Intents } from 'discord.js';
import { config } from './config.js';
import { getTokenValueInProducts } from './utils/helpers.js';
import { generateHeaders, generateJobIdUrl, generateUrl } from './utils/zapper.js';

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
// if you don't want to update a server ran by a bozo
const kekList = ['885769815552323626'];

let protocolsBalances = [];
let tokensBalances = [];
let totalUmami = 0.0;

const getAppsTokens = async () => {
  try {
    const headers = generateHeaders(config.ZAPPER_API_KEY);
    const { data } = await Axios.post(generateUrl('apps', config.ADDRESSES), undefined, {
      headers,
    });

    const jobId = data.jobId;
    // Poll for the job status to be 'complete'
    let jobStatus;
    do {
      const jobStatusResponse = await Axios.get(generateJobIdUrl(jobId), { headers });
      jobStatus = jobStatusResponse.data.status;
      // add delay to avoid overloading the server
      await new Promise((resolve) => setTimeout(resolve, 5000));
    } while (jobStatus !== 'completed');
    // Call the 'GET' endpoint to get the values
    const { data: balancesDatas } = await Axios.get(
      generateUrl('apps', config.ADDRESSES),
      { headers },
    );

    balancesDatas.forEach(({ appId, balanceUSD, products }) => {
      if (appId !== 'umami-finance') {
        const existingEntryIndex = protocolsBalances.findIndex(
          (item) => item.appId === appId,
        );
        const umamiBalanceUsdInProducts = getTokenValueInProducts(products, 'umami');

        if (existingEntryIndex > -1) {
          protocolsBalances[existingEntryIndex] = {
            ...protocolsBalances[existingEntryIndex],
            balanceUSD:
              protocolsBalances[existingEntryIndex].balanceUSD +
              balanceUSD -
              umamiBalanceUsdInProducts,
          };
        } else {
          protocolsBalances.push({
            appId,
            balanceUSD: balanceUSD - umamiBalanceUsdInProducts,
          });
        }
        totalUmami += umamiBalanceUsdInProducts;
      } else {
        totalUmami += balanceUSD;
      }
    });
  } catch (error) {
    console.error(error);
  }
};

const getWalletTokens = async () => {
  try {
    const headers = generateHeaders(config.ZAPPER_API_KEY);

    const { data: tokensJob } = await Axios.post(
      generateUrl('tokens', config.ADDRESSES),
      undefined,
      {
        headers,
      },
    );

    const jobId = tokensJob.jobId;
    // Poll for the job status to be 'complete'
    let jobStatus;
    do {
      const jobStatusResponse = await Axios.get(generateJobIdUrl(jobId), {
        headers,
      });
      jobStatus = jobStatusResponse.data.status;
      // add delay to avoid overloading the server
      await new Promise((resolve) => setTimeout(resolve, 5000));
    } while (jobStatus !== 'completed');
    // Call the 'GET' endpoint to get the values
    const { data: walletBalancesDatas } = await Axios.get(
      generateUrl('tokens', config.ADDRESSES),
      { headers },
    );

    Object.keys(walletBalancesDatas).map((walletAddress) => {
      const tokensList = walletBalancesDatas[walletAddress];
      tokensList.map((walletToken) => {
        const {
          address: tokenAddress,
          symbol: tokenSymbol,
          balanceUSD: tokenBalanceUSD,
        } = walletToken.token;
        if (!tokenSymbol.toLowerCase().includes('umami')) {
          const existingEntryIndex = tokensBalances.findIndex(
            (item) => item.address === tokenAddress,
          );
          if (existingEntryIndex > -1) {
            tokensBalances[existingEntryIndex] = {
              ...tokensBalances[existingEntryIndex],
              balanceUSD: tokensBalances[existingEntryIndex].balanceUSD + tokenBalanceUSD,
            };
          } else {
            tokensBalances.push({
              symbol: tokenSymbol,
              address: tokenAddress,
              balanceUSD: tokenBalanceUSD,
            });
          }
        } else {
          totalUmami += tokenBalanceUSD;
        }
      });
    });
  } catch (error) {
    console.error(error);
  }
};

export const updateValues = async () => {
  // Reset values to avoid up only
  protocolsBalances = [];
  tokensBalances = [];
  totalUmami = 0.0;
  await Promise.all([getWalletTokens(), getAppsTokens()]);

  const totalBalance =
    tokensBalances.reduce((acc, { balanceUSD }) => acc + balanceUSD, 0) +
    protocolsBalances.reduce((acc, { balanceUSD }) => acc + balanceUSD, 0);

  console.log('Total value : ', totalBalance + totalUmami);
  console.log('UMAMI removed : ', totalUmami);
  console.log('Update with : ', parseInt(totalBalance).toLocaleString('en-US'));
  client.guilds.cache.map((guild) => {
    // update every server the bot is connected to
    if (guild.me) {
      const nickname = kekList.includes(guild.id.toString())
        ? 'ON STRIKE'
        : `$${parseInt(totalBalance).toLocaleString('en-US')}`;
      guild.me.guild.me.setNickname(nickname);
      client.user.setActivity('Umami treasury', {
        type: 'WATCHING',
      });
    }
  });
};

const run = async () => {
  // update once to init then run every hour
  await updateValues();
  setInterval(updateValues, 60 * 60000);
};

client.once('ready', () => {
  console.log('Ready!');
  client.user.setActivity('Umami treasury', {
    type: 'WATCHING',
  });
  run();
});

client.login(config.DISCORD_CLIENT_TOKEN);
