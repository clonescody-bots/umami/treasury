import { config } from '../config.js';

export const generateUrl = (endpoint, addresses) => {
  let url = `${config.ZAPPER_API_BALANCES_URL}/${endpoint}?`;
  addresses.forEach((address, _index) => {
    url += `addresses%5B%5D=${address}${_index === addresses.length - 1 ? '' : '&'}`;
  });

  return url;
};

export const generateJobIdUrl = (jobId) =>
  `${config.ZAPPER_API_BALANCES_URL}/job-status?jobId=${jobId}`;

export const generateHeaders = (apiKey) => ({
  accept: '*/*',
  Authorization: `Basic ${Buffer.from(`${apiKey}:`, 'binary').toString('base64')}`,
});
