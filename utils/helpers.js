export const getTokenValueInProducts = (products, tokenSymbol) =>
  products.reduce(
    (productAcc, product) =>
      productAcc +
      product.assets.reduce(
        (assetAcc, asset) =>
          assetAcc +
          asset.tokens.reduce(
            (tokenAcc, token) =>
              tokenAcc + token.symbol.toLowerCase().includes(tokenSymbol.toLowerCase())
                ? token.balanceUSD
                : 0,
            0,
          ),
        0,
      ),
    0,
  );
