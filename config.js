export const config = {
  ZAPPER_API_BALANCES_URL: 'https://api.zapper.xyz/v2/balances',
  ZAPPER_API_KEY: process.env.ZAPPER_API_KEY,
  DISCORD_CLIENT_TOKEN: process.env.DISCORD_CLIENT_TOKEN,
  ARBI_RPC: process.env.ARBI_RPC,
  ADDRESSES: [
    '0xb137d135dc8482b633265c21191f50a4ba26145d',
    '0xb0b4bd94d656353a30773ac883591ddbabc0c0ba',
    '0x8e52ca5a7a9249431f03d60d79dda5eab4930178',
  ],
};
